﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationDemo.Datas;
using WebApplicationDemo.Datas.Services;
using WebApplicationDemo.Interface;
using WebApplicationDemo.Models.Config;
using WebApplicationDemo.Models.Entities;
using WebApplicationDemo.Models.Request;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplicationDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UsersService userService;

        public UsersController(ApplicationDbContext applicationDbContext, IOptions<Appsetting> options, ISessionData sessionData)
        {
            userService = new UsersService(applicationDbContext, options,sessionData);
        }
        // GET: api/<ValuesController>
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            try
            {
                var user = await userService.GetUserList();
                return new OkObjectResult(new { Data = user, StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message, StatusCode = 200 });
            }
        }

        [HttpGet]
        [Route("GetMe")]
        public async Task<IActionResult> GetMe()
        {
            try
            {
                var userId = User.Identity.Name;
                var user = await userService.GetMe(0);
                return new OkObjectResult(new { Data = user, StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message, StatusCode = 200 });
            }
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ValuesController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserRequest request)
        {
            try
            {
                var user = await userService.AddUser(request);
                return new OkObjectResult(new { Data = user, StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message, StatusCode = 200 });
            }
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            try
            {
                var user = await userService.GetUser(request);
                return new OkObjectResult(new { Data = user, StatusCode = 200 });
            }
            catch(Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message, StatusCode = 200 });
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserRequest request)
        {
            try
            {
                var user = await userService.EditUser(id,request);
                return new OkObjectResult(new { Data = user, StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message, StatusCode = 200 });
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await userService.DeleteUser(id);
                return new OkObjectResult(new { StatusCode = 200 });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { ex.Message });
            }
        }
    }
}
